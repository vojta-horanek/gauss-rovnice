
using System;

namespace gauss_rovnice
{
    static class Gauss
    {
        /* Uses gaussian elimination to calculate a linear equation
           returns array of results
           throws `FormatException` if the linear equatuion has no solution */
        public static double[] Eliminate(double[][] matrix)
        {
            int length = matrix[0].Length;

            for (int i = 0; i < matrix.Length - 1; i++)
            {
                if (matrix[i][i] == 0 && !Swap(matrix, i, i))
                {
                    throw new FormatException("No solution to this problem");
                }

                /* divide each row for substraction */
                for (int j = i; j < matrix.Length; j++)
                {
                    double[] newRow = new double[length];
                    for (int x = 0; x < length; x++)
                    {
                        newRow[x] = matrix[j][x];
                        if (matrix[j][i] != 0)
                        {
                            newRow[x] /= matrix[j][i];
                        }
                    }
                    matrix[j] = newRow;
                }

                /* subtract rows */
                for (int j = i + 1; j < matrix.Length; j++)
                {
                    double[] newRow = new double[length];
                    for (int x = 0; x < length; x++)
                    {
                        newRow[x] = matrix[j][x];
                        if (matrix[j][i] != 0)
                        {
                            newRow[x] -= matrix[i][x];
                        }

                    }
                    matrix[j] = newRow;
                }
            }

            /* return the result of the linear equation */
            return GaussResult(matrix);
        }

        /* swaps rows, returns true if rows swapped */
        private static bool Swap(double[][] matrix, int row, int column)
        {
            bool swapped = false;
            for (int i = matrix.Length - 1; i > row; i--)
            {
                if (matrix[i][row] != 0)
                {
                    double[] temp = new double[matrix[0].Length];
                    temp = matrix[i];
                    matrix[i] = matrix[column];
                    matrix[column] = temp;
                    swapped = true;
                }
            }

            return swapped;
        }

        /* returns result from gaussian elimination matrix 
           throws `FormatException` if the linear equatuion has no solution */
        private static double[] GaussResult(double[][] gausMat)
        {
            double val = 0;
            int length = gausMat[0].Length;
            double[] result = new double[gausMat.Length];
            for (int i = gausMat.Length - 1; i >= 0; i--)
            {
                val = gausMat[i][length - 1];
                for (int y = length - 2; y > i - 1; y--)
                {
                    val -= gausMat[i][y] * result[y];
                }
                /* probably the worst but quickest solution to number that end with .999999... */
                result[i] = Math.Round(val / gausMat[i][i], 4);

                if (double.IsNaN(result[i]) || double.IsInfinity(result[i]))
                {
                    throw new FormatException("No solution to this problem");
                }
            }
            return result;
        }
    }
}