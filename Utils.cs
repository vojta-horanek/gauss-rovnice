using System;
namespace gauss_rovnice
{
    static class Utils
    {
        /* returns a string from an array in the following format
           i1, i2, i3, i4
           where iX is an element of the array */
        public static string GetArrayString(double[] list)
        {
            string str = String.Empty;
            for (int i = 0; i < list.Length; i++)
            {
                str += "\t" + list[i].ToString();
                if (i != list.Length - 1)
                    str += Environment.NewLine;
            }
            return str;
        }

        /* reads user input and returns array of doubles representing the input
           throws `FormatException` if entered string is not parsable as a double */
        public static double[] GetUserInput()
        {
            string[] words = Console.ReadLine().Split(' ');
            double[] numbers = new double[words.Length];
            for (int i = 0; i < words.Length; i++)
            {
                double num;
                if (!double.TryParse(words[i], out num))
                {
                    throw new FormatException($"Number not valid `{words[i]}`");
                }
                numbers[i] = num;
            }
            return numbers;
        }
    }

}