﻿using System;

namespace gauss_rovnice
{
    class Program
    {
        static void Main(string[] args)
        {
            double[][] matrix;

            Console.WriteLine($"Hello {Environment.UserName}, format for input:");
            Console.WriteLine("\tFor `3x + 5y = -4`");
            Console.WriteLine("\tEnter `3 5 -4`");
            Console.WriteLine("Waiting for input...");

            try
            {
                var firstInput = Utils.GetUserInput();

                /* return if user only entered one value */
                if (firstInput.Length == 1)
                {
                    Console.WriteLine("At least two numbers required, exiting");
                    return;
                }

                /* initialize array with respective size */
                matrix = new double[firstInput.Length - 1][];
                matrix[0] = firstInput;

                /* get rest of the numbers */
                for (var i = 1; i < matrix.Length; i++)
                {
                    matrix[i] = Utils.GetUserInput();
                    if (matrix[i].Length != firstInput.Length)
                    {
                        Console.WriteLine("Input not accepted, missing one or more numbers, try again");
                        i--;
                    }
                }

                Console.WriteLine("Result:");
                Console.WriteLine(Utils.GetArrayString(Gauss.Eliminate(matrix)));
            }
            catch (Exception e)
            {
                /* catch any exception, just print the message */
                Console.WriteLine(e.Message);
            }
        }


    }
}